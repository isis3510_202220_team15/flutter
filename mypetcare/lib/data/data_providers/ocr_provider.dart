import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:tuple/tuple.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' show basename;

class OcrProvider{
  
  String scannedText = "";
  bool textScanning = false;
  

  Future pickImage(ImageSource source) async {
    print("Abre el Servicio abrir Camara");
      XFile? imageX = await ImagePicker().pickImage(source: source);
      print(imageX);
      if (imageX == null) return;
      String textoOCR = await getRecognisedText(imageX);
      File image = File(imageX.path);
      final imageTemporary = await saveImagePermanently(image.path);
    return Tuple2<File, String>(imageTemporary, textoOCR);
  }

  Future<String> getRecognisedText(XFile image) async {
    print("Enta OCR");
      final inputImage = InputImage.fromFilePath(image.path);
      final textDetector = GoogleMlKit.vision.textRecognizer();
      RecognizedText recognisedText = await textDetector.processImage(inputImage);
      await textDetector.close();
      scannedText = "";
      for (TextBlock block in recognisedText.blocks) {
        for (TextLine line in block.lines) {
          print("OCR working 13");
          scannedText = scannedText + line.text + "\n";
        }
      }
      return (scannedText);
    }

  Future<File> saveImagePermanently(String imagePath) async{
      final dir = await getApplicationDocumentsDirectory();
      final name = basename(imagePath);
      final image = File('${dir.path}/$name');  
      return File(imagePath).copy(image.path);
  }

}