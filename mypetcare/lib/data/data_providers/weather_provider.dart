import 'package:http/http.dart' as http;
import 'package:mypetcare/data/models/hourlyForecast.dart';

class WeatherProvider {
  static const String _baseURL = "weatherapi-com.p.rapidapi.com";
  static const _api_key = "c9859d0bf8msh548168e44cd2cecp18d9efjsnf3f7e5696fcb";
  static const String _endpoint = "/forecast.json";

  static const Map<String, String> _headers = {
    "content-type": "application/json",
    "X-RapidAPI-Host": "weatherapi-com.p.rapidapi.com",
    "X-RapidAPI-Key": _api_key,
  };

  Future<String> getWeatherData({required String location}) async {
    final queryParams = {"q": location, "days": "1"};
    Uri uri = Uri.https(_baseURL, _endpoint, queryParams);
    print(uri);
    final response = await http.get(uri, headers: _headers);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw Exception("Failed to load weather data");
    }
  }

  List<HourlyForecast> findBestHours(List<HourlyForecast> forecastPerH) {
    List<HourlyForecast> bestHours = [];
    List<int> goodCodes = [1000, 1003, 1006, 1009];
    for (var i = 6; i < 20; i++) {
      if (goodCodes.contains(forecastPerH[i].conditionCode)) {
        bestHours.add(forecastPerH[i]);
      }
    }
    return bestHours;
  }
}
