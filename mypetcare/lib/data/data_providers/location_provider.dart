import 'package:location/location.dart';

class LocationProvider {
  Location location = new Location();

  Future<String> getLocation() async {
    bool _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
    }

    PermissionStatus _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
    }

    var currentLocation = await location.getLocation();
    String q = currentLocation.latitude.toString() +
        "," +
        currentLocation.longitude.toString();
    return q;
  }
}
