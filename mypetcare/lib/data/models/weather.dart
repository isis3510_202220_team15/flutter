import 'package:mypetcare/data/models/hourlyForecast.dart';

class Weather {
  final double temperatureC;
  final hourlyForecasts;
  final String now;

  Weather(
      {this.temperatureC = 0,
      this.hourlyForecasts = List<HourlyForecast>,
      this.now = '2022-01-01 00:00:00.000'});

  //crear un objeto Weather a partir de un json que viene del api
  factory Weather.fromFullJson(Map<String, dynamic> json) {
    var fullForecast = json['forecast']['forecastday'] as List;
    var dayForcast = fullForecast[0];
    var hoursList = dayForcast['hour'] as List;
    var date = new DateTime.now().toString();

    return Weather(
        now: date,
        temperatureC: json['current']['temp_c'],
        hourlyForecasts: hoursList
            .map((data) => HourlyForecast.fromFullJson(data))
            .toList());
  }

  //crear un objeto Weather a partir de un json de cache
  factory Weather.fromJson(Map<String, dynamic> json) {
    var hoursList = json['hoursList'] as List;
    return Weather(
        now: json['now'],
        temperatureC: json['temperatureC'],
        hourlyForecasts:
            hoursList.map((data) => HourlyForecast.fromJson(data)).toList());
  }

  static Map<String, dynamic> toJson(Weather weather) => {
        'now': weather.now,
        'temperatureC': weather.temperatureC,
        'hoursList': weather.hourlyForecasts
            .map((hour) => HourlyForecast.toJson(hour))
            .toList()
      };
}
