import 'package:flutter/foundation.dart';

class HourlyForecast extends ChangeNotifier {
  final String date;
  final String conditionText;
  final int conditionCode;
  final double temperature;

  HourlyForecast({
    this.date = "2000-01-01 00:00",
    this.conditionCode = 0,
    this.conditionText = "Sunny",
    this.temperature = 0,
  });

  factory HourlyForecast.fromFullJson(Map<String, dynamic> json) {
    return HourlyForecast(
        date: json['time'],
        conditionCode: json['condition']['code'],
        conditionText: json['condition']['text'],
        temperature: json['temp_c']);
  }

  factory HourlyForecast.fromJson(Map<String, dynamic> json) {
    return HourlyForecast(
        date: json['time'],
        conditionCode: json['cond_code'],
        conditionText: json['cond_text'],
        temperature: json['temp_c']);
  }

  static Map<String, dynamic> toJson(HourlyForecast hour) => {
        'time': hour.date,
        'cond_code': hour.conditionCode,
        'cond_text': hour.conditionText,
        'temp_c': hour.temperature
      };
}
