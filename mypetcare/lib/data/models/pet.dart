import 'package:mypetcare/data/models/event.dart';

class Pet {
  final String name;
  final String breed;
  final String weight;
  final String age;
  final String Image;
  final events;

  Pet(
      {this.name = "",
      this.breed = "",
      this.age = "",
      this.weight = "",
      this.Image = "",
      this.events = List<Event> });
}
