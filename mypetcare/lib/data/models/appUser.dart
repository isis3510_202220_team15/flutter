import 'package:mypetcare/data/models/pet.dart';

class AppUser {
  final String fullName;
  final String email;
  final String birthDate;
  final String phoneNumber;
  final pets;

  AppUser(
      {this.fullName = "",
      this.email = "",
      this.birthDate = "",
      this.phoneNumber = "",
      this.pets = List<Pet>});
}
