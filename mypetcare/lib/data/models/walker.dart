class Walker {
  final String email;
  final String fullName;
  final String city;
  final String neighborhood;
  final String description;
  final List<dynamic> likes;

  Walker(
      {required this.email,
      required this.fullName,
      required this.city,
      required this.neighborhood,
      required this.description,
      required this.likes});
}
