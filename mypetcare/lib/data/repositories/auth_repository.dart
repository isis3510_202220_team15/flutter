import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mypetcare/data/models/appUser.dart';

class AuthRepository {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final firestore = FirebaseFirestore.instance;

  Future<User?> signUp(
      {required String email,
      required String password,
      required String fullName,
      required String birthDate,
      required String phoneNumber}) async {
    //Se crea el firebase auth por correo y contraseña
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);

      //Se crea el documento en la coleccion en firestore
      firestore.collection("users").doc(email).set({
        "name": fullName,
        "birthDate": birthDate,
        "phoneNumber": phoneNumber
      });
      User? usuario = _firebaseAuth.currentUser;
      return usuario;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        throw Exception('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        throw Exception('The account already exists for that email.');
      }
    } catch (e) {
      throw Exception(e.toString());
    }

    
  }

  Future<User?> login({
    required String email,
    required String password,
  }) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      User? usuario = _firebaseAuth.currentUser;
      return usuario;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw Exception('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        throw Exception('Wrong password provided for that user.');
      }
    }
  }

  Future<void> signOut() async {
    try {
      await _firebaseAuth.signOut();
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<AppUser?> findUserByEmail(email) async {
    AppUser? usuario = null;
    try {
      await firestore
          .collection("users")
          .doc(email)
          .get()
          .then((DocumentSnapshot ds) {
        usuario = AppUser(
          email: email,
          fullName: (ds.data() as dynamic)['name'],
          phoneNumber: (ds.data() as dynamic)['phoneNumber'],
          birthDate: (ds.data() as dynamic)['birthDate'],
        );
      });
      return usuario;
    } on FirebaseException catch (e) {
      rethrow;
    }
  }
}
