import 'dart:convert';

import 'package:json_cache/json_cache.dart';
import 'package:mypetcare/data/data_providers/location_provider.dart';
import 'package:mypetcare/data/data_providers/weather_provider.dart';
import 'package:mypetcare/data/models/hourlyForecast.dart';
import 'package:mypetcare/data/models/weather.dart';

class WeatherRepository {
  final WeatherProvider _weatherProvider = WeatherProvider();
  final LocationProvider _locationProvider = LocationProvider();
  final JsonCache jsonCache;

  WeatherRepository(this.jsonCache);

  Future<Weather?> getForecast(bool connected) async {
    var currently = new DateTime.now();
    Map<String, dynamic>? value = await jsonCache.value("weather_rec");
    /*
    casos:
    1. No está en caché y no hay internet => weather failure (mensaje de que no se puede)
    2. No está en caché pero sí hay internet => api call
    3. Está en caché, no hay internet => lo busca en caché
    4. Está en caché, hay internet =>
        4.1. Han pasado más de 15 minutos =>api call
        4.2 No han pasado más de 15 minutos => lo busca en caché
    */
    if (value == null && !connected) {
      return null;
    } else if (value == null && connected) {
      return callAPI();
    } else {
      //Está en caché
      print("está en cache");
      final weather = Weather.fromJson(value!);
      var sinceUpdate = currently.difference(DateTime.parse(weather.now));
      print(sinceUpdate.inMinutes);
      if (!connected || (connected && sinceUpdate.inMinutes < 15)) {
        return weather;
      } else {
        return callAPI();
      }
    }
  }

  Future<Weather> callAPI() async {
    ///obtener la ubicacion
    String location = await _locationProvider.getLocation();
    //obtener el weather object del dia
    String weatherJSON =
        await _weatherProvider.getWeatherData(location: location);
    //pasarlo de json al formato del weather del modelo
    Weather weather = Weather.fromFullJson(jsonDecode(weatherJSON));
    //guardarlo en cache
    Map<String, dynamic> myWeatherJson = Weather.toJson(weather);
    //print(myWeatherJson);
    jsonCache.refresh("weather_rec", myWeatherJson);
    return weather;
  }

  List<HourlyForecast> findBestHours(List<HourlyForecast> forecastPerH) {
    List<HourlyForecast> bestHours = [];
    List<int> goodCodes = [1000, 1003, 1006];
    for (var i = 6; i < 20; i++) {
      if (goodCodes.contains(forecastPerH[i].conditionCode)) {
        bestHours.add(forecastPerH[i]);
      }
    }
    //le pasamos al metodo que crea la recomendacion la lista de horas que sirven
    return bestHours;
  }

  //crear el mensaje de recomendacion
  String weatherMessage(bestHours) {
    String message = "";
    if (bestHours.isNotEmpty) {
      message = "Take your pets for a walk between the following hours:";
    } else {
      message = "The weather today may not be favorable to walk your pets";
    }
    return message;
  }
}
