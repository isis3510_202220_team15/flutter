import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mypetcare/data/models/walker.dart';

class WalkerRepository {
  final firestore = FirebaseFirestore.instance.collection("walkers");

  Future<Walker?> createWalker(
      {required String email,
      required String fullName,
      required String city,
      required String neighborhood,
      required String description}) async {
    //Se crea el documento en la coleccion en firestore
    try {
      firestore.doc(email).update({
        "name": fullName,
        "city": city,
        "neighborhood": neighborhood,
        "description": description
      });
      Walker? walker = await findWalkerByEmail(email);
      return walker;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<Walker?> findWalkerByEmail(email) async {
    Walker? walker = null;
    try {
      return await firestore.doc(email).get().then((DocumentSnapshot ds) {
        if (ds.exists) {
          return Walker(
              email: email,
              fullName: (ds.data() as dynamic)['name'],
              city: (ds.data() as dynamic)['city'],
              neighborhood: (ds.data() as dynamic)['neighborhood'],
              description: (ds.data() as dynamic)['description'],
              likes: (ds.data() as dynamic)['likes']);
        } else {
          return walker;
        }
      });
    } on FirebaseException catch (e) {
      rethrow;
    }
  }

  Future<void> findAllWalkers() async {}
}
