import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../models/event.dart';
class eventRepository {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final firestore = FirebaseFirestore.instance;


  Future<List> findUserByEmail(email) async {
    List usuario = [];
     await FirebaseFirestore.instance
          .collection("users")
          .doc(email)
          .get().then((ds) {
          usuario.add((ds.data() as dynamic)['name']);
          usuario.add((ds.data() as dynamic)['birthDate']);
          usuario.add((ds.data() as dynamic)['phoneNumber']);
     }).catchError((e){
      print(e);
     }); 
    return usuario;
  }

  Future<List> findPetByEmail(email, petName) async {
    List pet = [];
     await FirebaseFirestore.instance
          .collection("users")
          .doc(email).collection("mascotas").doc(petName)
          .get().then((ds) {
          pet.add((ds.data() as dynamic)['name']);
          pet.add((ds.data() as dynamic)['breed']);
          pet.add((ds.data() as dynamic)['age']);
          pet.add((ds.data() as dynamic)['weight']);

     }).catchError((e){
      print(e);
     }); 
    return pet;
  }
  
  void createEvent({required String name, 
  required String description,
  required String date,
  required String email,
  required String petName}){
    firestore.collection("users").doc(email).collection("mascotas").doc(petName).collection("eventos").doc(name).set({
      "name":name,
      "description":description,
      "date":date,
    });
  }
}