part of 'walker_bloc.dart';

@immutable
abstract class WalkerEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class CreationRequested extends WalkerEvent {
  final String email;
  final String fullName;
  final String city;
  final String neighborhood;
  final String description;

  CreationRequested(this.email, this.fullName, this.city, this.neighborhood,
      this.description);
}

class ConsultIfWalker extends WalkerEvent {
  final String email;

  ConsultIfWalker(this.email);
}
