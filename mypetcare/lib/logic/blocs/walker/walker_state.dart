part of 'walker_bloc.dart';

@immutable
abstract class WalkerState {}

class WalkerInitial extends WalkerState {}

class WalkerError extends WalkerState {
  final String error;

  WalkerError(this.error);
  @override
  List<Object?> get props => [error];
}

class WalkerLoading extends WalkerState {}

class WalkerChanged extends WalkerState {}

class WalkerCreated extends WalkerState {
  final Walker walker;
  WalkerCreated(this.walker);

  @override
  List<Object?> get props => [];
}
