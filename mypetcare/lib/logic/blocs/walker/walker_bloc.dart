import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:mypetcare/data/models/walker.dart';
import 'package:mypetcare/data/repositories/walker_repository.dart';

part 'walker_event.dart';
part 'walker_state.dart';

class WalkerBloc extends Bloc<WalkerEvent, WalkerState> {
  WalkerBloc() : super(WalkerInitial()) {
    on<CreationRequested>((event, emit) async {
      emit(WalkerLoading());
      try {
        Walker? walker = await WalkerRepository().createWalker(
            email: event.email,
            fullName: event.fullName,
            city: event.city,
            neighborhood: event.neighborhood,
            description: event.description);
        emit(WalkerChanged());
        emit(WalkerCreated(walker!));
      } catch (e) {
        emit(WalkerError(e.toString()));
        emit(WalkerInitial());
      }
    });

    on<ConsultIfWalker>((event, emit) async {
      try {
        Walker? walker =
            await WalkerRepository().findWalkerByEmail(event.email);
        if (walker != null) {
          print(walker.city);
          emit(WalkerCreated(walker));
        }
      } catch (e) {
        print(e.toString());
      }
    });
  }
}
