part of 'ocr_bloc.dart';

@immutable
abstract class OcrEvent extends Equatable {
  @override
  List<Object> get props => [];
}

/*
Cuando se entra a la main page se pide el weather 
*/
class OcrRequestedOnPhoto extends OcrEvent {}

class OcrRequestedOnImage extends OcrEvent {}

