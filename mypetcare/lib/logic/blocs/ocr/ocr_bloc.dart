
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';
import 'package:mypetcare/data/data_providers/ocr_provider.dart';
import 'package:mypetcare/logic/cubits/connectivity/connectivity_cubit.dart';
import 'package:tuple/tuple.dart';

part 'ocr_event.dart';
part 'ocr_state.dart';

class OcrBloc extends Bloc<OcrEvent, OcrState> {
  
  OcrBloc() : super(beforeImage()) {
    on<OcrRequestedOnPhoto>((event, emit) async {
      try {
      Tuple2<File,String> ocrResponse = await OcrProvider().pickImage(ImageSource.camera);
      String recognizedText =  ocrResponse.item2;
      File image = ocrResponse.item1;
      emit(ImageObtained(recognizedText,image, 0));

      }catch (e){
              emit(ImageFailure());

      }
    });

    on<OcrRequestedOnImage>((event, emit) async {
      try {
        Tuple2<File,String> ocrResponse = await OcrProvider().pickImage(ImageSource.gallery);
        String recognizedText =  ocrResponse.item2;
        File image = ocrResponse.item1;
        emit(ImageObtained(recognizedText,image, 1));


      }catch (e){
        emit(ImageFailure());

      }
    });
  }
}
