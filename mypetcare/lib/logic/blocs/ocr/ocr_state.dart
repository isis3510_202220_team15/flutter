part of 'ocr_bloc.dart';

@immutable
abstract class OcrState extends Equatable {}

class beforeImage extends OcrState {
  @override
  List<Object?> get props => [];
}

class ImageFailure extends OcrState {
  @override
  List<Object?> get props => [];
}

class ImageObtained extends OcrState {
  final String recognizedText;
  final File image;
  final int i;

  ImageObtained(this.recognizedText, this.image, this.i);
  @override
  List<Object?> get props => [];
}
