part of 'events_bloc.dart';

@immutable
abstract class EventsState extends Equatable {}

class beforeEventCreation extends EventsState{
  @override
  List<Object?> get props => [];
}

class eventFailure extends EventsState {
  @override
  List<Object?> get props => [];
}

class eventCreated extends EventsState{
  final String name;
  final String description;
  final String date;

  eventCreated(this.name, this.description, this.date); 
  @override
  List<Object?> get props => [];
  
}