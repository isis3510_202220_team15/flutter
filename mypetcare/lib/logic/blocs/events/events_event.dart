part of 'events_bloc.dart';

@immutable
abstract class EventsEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class EventcreationRequested extends EventsEvent{
  final String name;
  final String description;
  final String date;
  EventcreationRequested(this.name, this.description, this.date);
}
