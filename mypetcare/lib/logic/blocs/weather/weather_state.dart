part of 'weather_bloc.dart';

@immutable
abstract class WeatherState extends Equatable {}

class WeatherLoading extends WeatherState {
  @override
  List<Object?> get props => [];
}

class WeatherFailure extends WeatherState {
  @override
  List<Object?> get props => [];
}

class WeatherObtained extends WeatherState {
  final String temperatureC;
  final String weatherRecommendation;
  final List<HourlyForecast> bestHours;
  final String lastUpdate;

  WeatherObtained(this.temperatureC, this.weatherRecommendation, this.bestHours,
      this.lastUpdate);
  @override
  List<Object?> get props => [];
}
