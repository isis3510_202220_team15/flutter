import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:json_cache/json_cache.dart';
import 'package:localstorage/localstorage.dart';
import 'package:meta/meta.dart';
import 'package:mypetcare/data/models/hourlyForecast.dart';
import 'package:mypetcare/data/models/weather.dart';
import 'package:mypetcare/data/repositories/weather_repository.dart';
import 'package:mypetcare/logic/cubits/connectivity/connectivity_cubit.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final ConnectivityCubit connectivityCubit;
  bool connected = true;
  WeatherRepository _weatherRepository = WeatherRepository(
      JsonCacheMem(JsonCacheLocalStorage(LocalStorage('weather.json'))));

  WeatherBloc({required this.connectivityCubit}) : super(WeatherLoading()) {
    StreamSubscription internetStreamSubscription =
        connectivityCubit.stream.listen((internetState) {
      if (internetState is InternetConnected) {
        connected = true;
        print("Internet? " + connected.toString());
      } else if (internetState is InternetDisconnected) {
        connected = false;
        print("Internet? " + connected.toString());
      }
    });

    on<WeatherRequested>((event, emit) async {
      emit(WeatherLoading());
      try {
        Weather? weather = await _weatherRepository.getForecast(connected);
        if (weather != null) {
          //si se pudo obtener la recomendación
          List<HourlyForecast> bestHours =
              _weatherRepository.findBestHours(weather.hourlyForecasts);
          String weatherRecommendation =
              _weatherRepository.weatherMessage(bestHours);
          emit(WeatherObtained(weather.temperatureC.toString(),
              weatherRecommendation, bestHours, weather.now));
        } else {
          //si no se pudo se muestra mensaje de error en la ui
          emit(WeatherFailure());
        }
      } catch (e) {
        emit(WeatherFailure());
      }
    });

    @override
    Future<void> close() {
      internetStreamSubscription.cancel();
      return super.close();
    }
  }
}
