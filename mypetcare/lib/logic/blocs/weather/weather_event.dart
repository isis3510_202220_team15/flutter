part of 'weather_bloc.dart';

@immutable
abstract class WeatherEvent extends Equatable {
  @override
  List<Object> get props => [];
}

/*
Cuando se entra a la main page se pide el weather 
*/
class WeatherRequested extends WeatherEvent {}
