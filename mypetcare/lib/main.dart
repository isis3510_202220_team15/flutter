import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:mypetcare/logic/blocs/authentication/authentication_bloc.dart';
import 'package:mypetcare/logic/blocs/walker/walker_bloc.dart';
import 'package:mypetcare/logic/blocs/weather/weather_bloc.dart';
import 'package:mypetcare/logic/cubits/connectivity/connectivity_cubit.dart';
import 'package:mypetcare/presentation/router/app_router.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  Firebase.initializeApp().then((value) {
    SentryFlutter.init((options) {
      options.dsn =
          'https://8d5830f37e69487e978e1122961778a4@o4503937867841536.ingest.sentry.io/4503937869938688';
      options.tracesSampleRate = 1.0;
    });
  }).then((value) {
    runApp(MyApp());
  });
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final AppRouter _appRouter = AppRouter();
  final Connectivity connectivity = Connectivity();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ConnectivityCubit>(
            create: (context) => ConnectivityCubit(connectivity: connectivity)),
        BlocProvider<AuthenticationBloc>(
            create: (context) => AuthenticationBloc()),
        BlocProvider<WalkerBloc>(create: (context) => WalkerBloc()),
        BlocProvider<WeatherBloc>(
            create: (context) => WeatherBloc(
                connectivityCubit:
                    BlocProvider.of<ConnectivityCubit>(context))),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        onGenerateRoute: _appRouter.onGenerateRoute,
      ),
    );
  }
}
