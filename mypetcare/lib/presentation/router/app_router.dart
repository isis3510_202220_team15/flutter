import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mypetcare/logic/blocs/ocr/ocr_bloc.dart';
import 'package:mypetcare/logic/blocs/walker/walker_bloc.dart';
import 'package:mypetcare/logic/blocs/weather/weather_bloc.dart';
import 'package:mypetcare/presentation/screens/addEvent_view.dart';

import 'package:mypetcare/presentation/screens/createWalker_view.dart';
import 'package:mypetcare/presentation/screens/documents_view.dart';
import 'package:mypetcare/presentation/screens/login_view.dart';
import 'package:mypetcare/presentation/screens/home_view.dart';
import 'package:mypetcare/presentation/screens/petProfile_view.dart';
import 'package:mypetcare/presentation/screens/recommendations_view.dart';
import 'package:mypetcare/presentation/screens/signUp_view.dart';
import 'package:mypetcare/presentation/screens/userProfile_view.dart';
import 'package:mypetcare/presentation/screens/walkers_view.dart';

class AppRouter {
  final OcrBloc _ocrBloc = OcrBloc();


  MaterialPageRoute? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
          builder: (_) => LoginView(),
        );
      case '/signUp':
        return MaterialPageRoute(
          builder: (_) => SignUpView(),
        );
      case '/home':
        return MaterialPageRoute(
          builder: (_) => HomeView(),
        );
      case '/userProfile':
        return MaterialPageRoute(
          builder: (_) => UserProfileView(),
        );
      case '/petProfile':
        return MaterialPageRoute(
          builder: (_) => PetProfileView(),
        );
      case '/recommendations':
        return MaterialPageRoute(
          builder: (_) => RecommendationView(),
        );
      case '/documents':
        return MaterialPageRoute(
          builder: (_) => BlocProvider.value(
                  value: _ocrBloc,
                  child: DocumentsView(),
                ));
      case '/createWalker':
        return MaterialPageRoute(
          builder: (_) => CreateWalkerView(),
        );

      case '/addEvent':
        return MaterialPageRoute(
          builder: (_) => addEventView(),
        );
      case '/exploreWalkers':
        return MaterialPageRoute(
          builder: (_) => WalkersView(),

        );
      default:
        return null;
    }
  }
}
