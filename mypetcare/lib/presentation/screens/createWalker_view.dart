import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mypetcare/logic/blocs/authentication/authentication_bloc.dart';
import 'package:mypetcare/logic/blocs/walker/walker_bloc.dart';

class CreateWalkerView extends StatefulWidget {
  const CreateWalkerView({Key? key}) : super(key: key);

  @override
  _CreateWalkerViewState createState() => _CreateWalkerViewState();
}

class _CreateWalkerViewState extends State<CreateWalkerView> {
  @override
  void initState() {
    super.initState();
    findUser();
  }

  String _email = '';
  String _fullName = '';
  String _city = '';
  String _neighborhood = '';
  String _description = '';
  String _mode = 'Create';
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(12, 116, 137, 1),
          title: const Text("Walker Profile"),
        ),
        body: BlocConsumer<WalkerBloc, WalkerState>(listener: (context, state) {
          if (state is WalkerError) {
            // Showing the error message if the user has entered invalid credentials
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(state.error)));
          }
          if (state is WalkerChanged) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text('Walker profile successfully ' +
                    _mode.toLowerCase() +
                    'd!')));
          }
        }, builder: (context, state) {
          if (state is WalkerLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is WalkerCreated || state is WalkerInitial) {
            return displayForm();
          }
          return Container();
        }));
  }

  void findUser() {
    var authState = BlocProvider.of<AuthenticationBloc>(context).state;
    if (authState is Authenticated) {
      _email = authState.email;
      _fullName = authState.usuario.fullName;
    }
    var walkerState = BlocProvider.of<WalkerBloc>(context).state;
    if (walkerState is WalkerCreated) {
      _mode = 'Update';
      _city = walkerState.walker.city;
      _neighborhood = walkerState.walker.neighborhood;
      _description = walkerState.walker.description;
    }
  }

  String titleText() {
    if (_mode == 'Create') {
      return 'Become a walker!';
    } else {
      return 'Your walker information';
    }
  }

  Widget displayForm() {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: Container(
                    width: 250,
                    height: 150,
                    child: Text(titleText(),
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.black87, fontSize: 40))),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "E-mail",
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintText: _email,
                ),
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Name",
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    hintText: _fullName),
                enabled: false,
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),

              child: TextFormField(
                maxLength: 35,
                obscureText: false,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'City',
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    hintText: _city),
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Can\'t be empty';
                  }
                  if (text.length < 2) {
                    return 'Too short';
                  }
                  return null;
                },
                onChanged: (text) => setState(() => _city = text),
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                maxLength: 35,
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Neighborhood',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintText: _neighborhood,
                ),
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Can\'t be empty';
                  }
                  if (text.length < 2) {
                    return 'Too short';
                  }
                  return null;
                },
                onChanged: (text) => setState(() => _neighborhood = text),
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                maxLines: 3,
                maxLength: 130,
                obscureText: false,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Description',
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    hintText: _description),
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Can\'t be empty';
                  }
                  return null;
                },
                onChanged: (text) => setState(() => _description = text),
              ),
            ),
            Container(
              height: 50,
              width: 250,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      Color.fromRGBO(12, 116, 137, 1),
                    ),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)))),
                onPressed: () {
                  _createWalker(context);
                },
                child: Text(
                  _mode + ' profile',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _createWalker(context) {
    if (_formKey.currentState!.validate() ||
        (_city.isNotEmpty &&
            _neighborhood.isNotEmpty &&
            _description.isNotEmpty)) {
      // If email is valid adding new event [SignUpRequested].
      BlocProvider.of<WalkerBloc>(context).add(
        CreationRequested(
            _email, _fullName, _city, _neighborhood, _description),
      );
    }
  }
}
