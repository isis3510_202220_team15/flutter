import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mypetcare/logic/blocs/authentication/authentication_bloc.dart';

class HamburgerMenu extends StatefulWidget {
  @override
  _HamburgerMenuState createState() => _HamburgerMenuState();
}

class _HamburgerMenuState extends State<HamburgerMenu> {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Container(
        color: Color.fromRGBO(12, 116, 137, 1),
        height: 200,
        padding: EdgeInsets.only(top: 20),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Container(
              height: 80,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage("assets/icons/general/dog.png"),
                ),
              )),
          Text(
            "Welcome!",
            style: TextStyle(color: Colors.white, fontSize: 20),
          )
        ]),
      ),
      Container(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text("Home"),
              leading: Icon(Icons.home),
              onTap: () {
                Navigator.of(context).pushNamed('/home');
              },
            ),
            ListTile(
              title: Text("Documents"),
              leading: Icon(Icons.document_scanner),
              onTap: () {
                Navigator.of(context).pushNamed('/documents');
              },
            ),
            ListTile(
              title: Text("Pet profile"),
              leading: Icon(Icons.pets),
              onTap: () {
                Navigator.of(context).pushNamed('/petProfile');
              },
            ),
            ListTile(
              title: Text("Pet Recommendations"),
              leading: Icon(Icons.recommend),
              onTap: () {
                Navigator.of(context).pushNamed('/recommendations');
              },
            ),
            ListTile(
              title: Text("Walker profile"),
              leading: Icon(Icons.directions_walk_rounded),
              onTap: () {
                Navigator.of(context).pushNamed('/createWalker');
              },
            ),
            ListTile(
              title: Text("Explore walkers"),
              leading: Icon(Icons.person_search_rounded),
              onTap: () {
                Navigator.of(context).pushNamed('/exploreWalkers');
              },
            ),
            ListTile(
              title: Text("Logout"),
              leading: Icon(Icons.logout),
              onTap: () {
                _logout();
                Navigator.of(context).pushNamed('/');
              },
            ),
          ],
        ),
      ),
    ]);
  }

  void _logout() {
    BlocProvider.of<AuthenticationBloc>(context).add(SignOutRequested());
  }
}
