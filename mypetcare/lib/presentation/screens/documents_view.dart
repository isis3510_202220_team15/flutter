// ignore_for_file: library_private_types_in_public_api

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mypetcare/data/data_providers/ocr_provider.dart';
import 'package:mypetcare/logic/blocs/ocr/ocr_bloc.dart';

class DocumentsView extends StatefulWidget {
  @override
  _DocumentsViewState createState() => _DocumentsViewState();
}

class _DocumentsViewState extends State<DocumentsView> {

  int _selectedIndex = 0;
  File? image; 
  String recognizedText = ''; 
 @override
  void initState() {
    super.initState();
  }

 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(12, 116, 137, 1),
        title: const Text('Pet recommendations'),
      ),

      body: BlocBuilder<OcrBloc, OcrState>(
        builder: (context, state) {
          if (state is ImageObtained){
            recognizedText = state.recognizedText;
            return  Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                _textHeader("Rex"),
                
                Image.file(
                    state.image,
                    width: 300,
                    height: 300,
                ),
                Card(
                  margin: EdgeInsets.only(top:25),
                  child: InkWell(
                    splashColor: Colors.blue.withAlpha(30),
                    onTap: () {
                      debugPrint('Card tapped.');
                    },
                    child:  SizedBox(
                      width: 300,
                      height: 200,
                      child:  SingleChildScrollView(
                        child: ListTile(
                            title: Text('OCR text generated'),
                            subtitle: Text( recognizedText,  style: TextStyle(fontSize: 16),
                          ),
                      )
                      )
                    ),
                  ),
                ), ]));
          }else{
            recognizedText = "";
            return  Container(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  _textHeader("Rex"),
                  Container(
                        width: 300,
                        height: 300,
                        color: Colors.grey[300]!
                  ),
                  Card(
                    margin: EdgeInsets.only(top:25) ,
                    
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        debugPrint('Card tapped.');
                      },
                      child:  SizedBox(
                        width: 300,
                        height: 100,
                        child:  SingleChildScrollView(
                          child: ListTile(
                              title: Text('OCR text generated'),
                              subtitle: Text( recognizedText,  style: TextStyle(fontSize: 16),
                            ),
                        )
                        )
                      ),
                    ),
                  ),]));

          }
      },
    ),
    bottomNavigationBar: _bottomBar(),);
  }

  BottomNavigationBar _bottomBar() {
    return BottomNavigationBar(
      items: const [
        BottomNavigationBarItem(
            icon: Icon(Icons.document_scanner), label: "Scan"),
        BottomNavigationBarItem(icon: Icon(Icons.upload_file), label: "Upload"),
        BottomNavigationBarItem(icon: Icon(Icons.center_focus_strong), label: "OCR",),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.amber[800],
      onTap: _onItemTapped,
    );
  }
  
 Future<void> _onItemTapped(int index) async {
    switch (index) {
      case 0:
        if (_selectedIndex == index) {
          
            BlocProvider.of<OcrBloc>(context).add(OcrRequestedOnPhoto());

        }
          
          break;
        case 1:
          if (_selectedIndex == index) {
            BlocProvider.of<OcrBloc>(context).add(OcrRequestedOnImage());

          }
          break;
        case 2:

          break;
      }
      setState ((){ _selectedIndex = index;});
    

  }


  Widget _textHeader(texto) {
    return Row(children: <Widget>[
      Text(
        texto,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 30,
        ),
      )
    ]);
  }

}
