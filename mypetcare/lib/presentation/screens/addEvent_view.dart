import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/src/intl/date_format.dart';
import 'package:mypetcare/data/repositories/events_repository.dart';
import 'package:mypetcare/logic/blocs/authentication/authentication_bloc.dart';


class addEventView extends StatefulWidget {
  @override
  _addEventView createState() => _addEventView();
}

class _addEventView extends State<addEventView>{
  String _name= "";
  String _description = "";
  String _date = "";
  String _email = "a@icloud.com";
  String _petName = ""; 
  eventRepository eventManager = eventRepository();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        backgroundColor: Color.fromRGBO(12, 116, 137, 1),
        title: const Text('Add Event'),
      ),
      body:BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context,state){ 
          if (state is Authenticated){
            return Center(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    _petimage(),
                      const SizedBox(
                              height: 30,
                            ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: TextFormField(
                          maxLength: 30,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Name',
                          ),
                          validator: (text) {
                            if (text == null || text.isEmpty) {
                              return 'Can\'t be empty';
                            }
                            if (text.length < 4) {
                              return 'Too short';
                            }
                            return null;
                          },
                          onChanged: (text) => setState(() => _name = text),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: TextFormField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Description',
                          ),
                          validator: (text) {
                            if (text == null || text.isEmpty) {
                              return 'Can\'t be empty';
                            }
                            if (text.length > 300) {
                              return 'Too long';
                            }
                            return null;
                          },
                          onChanged: (text) => setState(() => _description = text),
                        ),
                      ),
                        Padding(
                              padding: EdgeInsets.only(
                                  left: 15.0, right: 15.0, top: 10, bottom: 15),
                              child: TextFormField(
                                controller: TextEditingController(text: _date),
                                decoration: InputDecoration(
                                    icon:
                                        Icon(Icons.calendar_today), //icon of text field
                                    labelText: "Date" //label text of field
                                    ),
                                validator: (text) {
                                  if (text == null || text.isEmpty) {
                                    return 'Can\'t be empty';
                                  }
                                  return null;
                                },
                                readOnly: true,
                                //set it true, so that user will not able to edit text
                                onTap: () async {
                                  DateTime? pickedDate = await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime.now(),
                                      //DateTime.now() - not to allow to choose before today.
                                      lastDate: DateTime(2025));
              
                                  if (pickedDate != null) {
                                    print(pickedDate); //ej=> 2021-03-10 00:00:00.000
                                    String formattedDate =
                                        DateFormat('dd/MM/yy').format(pickedDate);
                                    print(formattedDate); //ej=>  03/10/21
                                    setState(() {
                                      _date =
                                          formattedDate; //set output date to TextField value.
                                    });
                                  }
                                },
                              ),
                            ),
                            Container(
                              height: 50,
                              width: 250,
                              child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                      Color.fromRGBO(12, 116, 137, 1),
                                    ),
                                    shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(100)))),
                                onPressed: () {
                                    _uploadEvent(context );
              
                                },
                                child: const Text(
                                  'Save',
                                  style: TextStyle(color: Colors.white, fontSize: 25),
                                ),
                              ),
                            ),
                          ],),
              )
            );
          }
        return Container();
        }
        )
    );
  }

  Widget _petimage() {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 10.0),
        child: Container(
            decoration: _boxDecorationPet(),
            height: 150,
            width: 150,
            child: Column(children: <Widget>[
              Image(
                  image: NetworkImage(
                "https://cdn-icons-png.flaticon.com/512/194/194177.png",
              ))
            ])));
  }
  BoxDecoration _boxDecorationPet() {
      return BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(100.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            offset: Offset(2.0, 2.0),
            blurRadius: 8.0,
          ),
        ],
      );
    }


  _uploadEvent(context){

    eventManager.createEvent(name: _name, description: _description, date: _date, email: _email, petName: _petName);
  
  }

}