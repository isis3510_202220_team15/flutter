// ignore_for_file: library_private_types_in_public_apiLoginView
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:email_validator/email_validator.dart';
import 'package:mypetcare/logic/blocs/authentication/authentication_bloc.dart';
import 'package:mypetcare/logic/blocs/walker/walker_bloc.dart';
import 'package:mypetcare/logic/cubits/connectivity/connectivity_cubit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  String _email = '';
  String? _password = '';
  bool connect = false;
  final _formKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(12, 116, 137, 1),
          title: const Text("MY PET CARE"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              MultiBlocListener(
                  listeners: [
                  BlocListener<AuthenticationBloc, AuthenticationState>(
                    listener: (context, state) {
                        if (state is Authenticated) {
                        Navigator.of(context).pushNamed('/home');
                        BlocProvider.of<WalkerBloc>(context).add(
                        ConsultIfWalker(_email),
                          );
                        }
                        if (state is AuthError) {
                          // Showing the error message if the user has entered invalid credentials
                          ScaffoldMessenger.of(context)
                              .showSnackBar(SnackBar(content: Text(state.error)));
                        }
                    }),
                     BlocListener<ConnectivityCubit, ConnectivityState>(
                    listener: (context, state) {
                        if (state is InternetDisconnected) {
                          connect = false;
                        ScaffoldMessenger.of(context)
                              .showSnackBar(SnackBar(content: Text('No internet connection')));
                        }else{connect = true;}                      
                    }),
                     BlocListener<ConnectivityCubit, ConnectivityState>(
                      listenWhen: (previous,current){
                        return previous is InternetDisconnected && current is InternetConnected;
                      },
                      listener: (context, state) {
                        if (state is InternetDisconnected) {
                           connect = false;
                        ScaffoldMessenger.of(context)
                              .showSnackBar(SnackBar(content: Text('You are now connected to the internet')));
                        }else{connect = true;}
                    }),
                    ],
                    child: SizedBox.shrink(),
                ),
             
           BlocBuilder<AuthenticationBloc, AuthenticationState>(
             builder: (context, state) {
            if (state is AuthLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is UnAuthenticated) {
              return 
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 60.0),
                        child: Center(
                          child: Container(
                              width: 250,
                              height: 150,
                              /*decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(50.0)),*/
                              child: const Text('Welcome to My Pet Care!',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.black87, fontSize: 40))),
                        ),
                      ),
                      Padding(
                        //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: TextFormField(
                          maxLength: 35,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Email',
                            hintText: _email,
                            floatingLabelBehavior: FloatingLabelBehavior.always,
                          ),
                          validator: (text) {
                            text=_email;
                            if ( text.isEmpty) {
                              return 'Can\'t be empty';
                            }
                            if (text.length < 4) {
                              return 'Too short';
                            }
                            if (!EmailValidator.validate(text)) {
                              return 'Must be a valid email address';
                            }
                            return null;
                          },
                          onChanged: (text) => setState(() => _email = text),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 15.0, right: 15.0, top: 15, bottom: 0),
                        child: TextFormField(
                          maxLength: 20,
                          obscureText: true,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Password',
                            floatingLabelBehavior: FloatingLabelBehavior.always,
                          ),
                          validator: (text) {
                            if (text == null || text.isEmpty) {
                              return 'Can\'t be empty';
                            }
                            if (text.length < 4) {
                              return 'Too short';
                            }
                            return null;
                          },
                          onChanged: (text) => setState(() => _password = text),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          //TODO FORGOT PASSWORD SCREEN GOES HERE
                        },
                        child: const Text(
                          'I forgot my password',
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.blueGrey, fontSize: 15),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: 250,
                        child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                Color.fromRGBO(12, 116, 137, 1),
                              ),
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100)))),
                          onPressed: () {
                            if(connect == true){
                            _authenticateWithEmailAndPassword(context);
                              //Guardar informacion localmente
                                saveData(_email, _password);
                            }
                            else{
                              ScaffoldMessenger.of(context)
                                .showSnackBar(SnackBar(content: Text('You don\'t have internet connection. \n Check your internet connection and try again')));
                            }                                  
                          },
                          child: const Text(
                            'Login',
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 130,
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed('/signUp');
                        },
                        child: const Text(
                          "Don't have an account? Sign Up here!",
                          style: TextStyle(color: Colors.grey, fontSize: 15),
                        ),
                      ),
                    ],
                  ),
                );
              }
              return SizedBox.shrink();
              }
          )],
          ),
        ));
  }

  void _authenticateWithEmailAndPassword(context) {
    if (_formKey.currentState!.validate() ) {
      // If email is valid adding new Event [SignInRequested].
      BlocProvider.of<AuthenticationBloc>(context).add(
        LoginRequested(_email, _password!),
      );
    }
  }

  Future<void> saveData(email, password) async{
    SharedPreferences _prefs = await  SharedPreferences.getInstance();
      await _prefs.setString("email", email);
      await _prefs.setString("password", password);
  }
 
 

  Future<void> loadPreferences() async{
    SharedPreferences _prefs = await  SharedPreferences.getInstance();
    _email  = (await _prefs.getString("email"))!; 
    _password  = await _prefs.getString("password");
    
    
  }


  @override
  void initState(){
    loadPreferences();
    super.initState();
    
    
    
    
    
  }
}
