import 'dart:io';
import 'package:mypetcare/presentation/screens/documents_view.dart';
import 'package:path/path.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

class PetProfileView extends StatefulWidget {
  @override
  _PetProfileViewState createState() => _PetProfileViewState();
}

class _PetProfileViewState extends State<PetProfileView> {
  bool showPassword = false;
  File? image;
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(12, 116, 137, 1),
        title: const Text('Pet'),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 16, top: 25, right: 16),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              const SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 15,
              ),
              Center(
                child: Stack(
                  children: [
                    Container(
                      width: 150,
                      height: 150,
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 4,
                              color: Theme.of(context).scaffoldBackgroundColor),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 2,
                                blurRadius: 10,
                                color: Colors.black.withOpacity(0.1),
                                offset: Offset(0, 10))
                          ],
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                "https://cdn-icons-png.flaticon.com/512/194/194177.png",
                              ))),
                    ),
                    Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                width: 4,
                                color:
                                    Theme.of(context).scaffoldBackgroundColor,
                                style: BorderStyle.none),
                          ),
                          child: Icon(
                            Icons.edit,
                            color: Colors.black,
                          ),
                        )),
                  ],
                ),
              ),
              SizedBox(
                height: 35,
              ),
              buildTextField("Name", "Rex"),
              buildTextField("Breed", "Labrador"),
              buildTextField("Age", "3 years old"),
              buildTextField("Weight", "21 kg"),
              SizedBox(
                height: 25,
              ),
              _addEvents(),
              Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Text(
                    "Documents",
                    style: TextStyle(fontSize: 20),
                  )),
              _documentsWidget(context),
            ],
          ),
        ),
      ),

      //Segunda Manera
      //bottomNavigationBar: _bottomBar()
    );
  }

  Widget buildTextField(String labelText, String placeholder) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextField(
        decoration: InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(223, 213, 236, 100),
            contentPadding: EdgeInsets.only(bottom: 3, left: 5, top: 3),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            )),
      ),
    );
  }

  BottomNavigationBar _bottomBar() {
    return BottomNavigationBar(
      elevation: 1,
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.upload), label: "Update"),
        BottomNavigationBarItem(icon: Icon(Icons.folder), label: "Documents"),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.amber[800],
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index) {
    switch (index) {
      case 0:
        if (_selectedIndex == index) {
          pickImage(ImageSource.camera);
        }
        break;
      case 1:
        if (_selectedIndex == index) {
          //Deberia cambiar de pantalla

        }
        break;
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  Future pickImage(ImageSource source) async {
    final image = await ImagePicker().pickImage(source: source);
    if (image == null) return;
    final imagePermanet = saveImagePermanently(image.path);
    setState(() => this.image = imagePermanet as File?);
    this.image = imagePermanet as File?;
  }

  Future saveImagePermanently(String imagePath) async {
    final directory = await getApplicationDocumentsDirectory();
    final name = basename(imagePath);
    final image = File('${directory.path}/$name');

    return File(imagePath).copy(imagePath);
  }

  Widget _addEvents() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text("Upcoming Appointments", style: TextStyle(fontSize: 18)),
      ],
    );
  }

  Widget _documentsWidget(context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              margin: EdgeInsets.all(25),
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      Color.fromRGBO(12, 116, 137, 1),
                    ),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)))),
                onPressed: () => ImagePicker(),
                child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Icon(Icons.upload_file)),
              )),
          Container(
              margin: EdgeInsets.all(25),
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      Color.fromRGBO(12, 116, 137, 1),
                    ),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)))),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (_) => DocumentsView()));
                },
                child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Icon(Icons.file_download)),
              ))
        ],
      ),
    );
  }
}
