// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';

class WalkersView extends StatefulWidget {
  @override
  _WalkersViewState createState() => _WalkersViewState();
}

class _WalkersViewState extends State<WalkersView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(12, 116, 137, 1),
        title: const Text('Explore walkers'),
      ),
    );
  }
}
