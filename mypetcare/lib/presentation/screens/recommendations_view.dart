// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class RecommendationView extends StatefulWidget {
  const RecommendationView({Key? key}) : super(key: key);

  @override
  _RecommendationViewState createState() => _RecommendationViewState();
}

class _RecommendationViewState extends State<RecommendationView> {
  final List<String> petsList = [
    'assets/icons/general/dog.png',
    'assets/icons/general/cat.png'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(12, 116, 137, 1),
        title: const Text('Pet recommendations'),
      ),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          Container(
              padding: EdgeInsets.only(top: 10.0),
              height: 150,
              child: CarouselSlider(
                options: CarouselOptions(
                  autoPlay: false,
                ),
                items: petsList
                    .map(
                      (item) => Image(
                        image: AssetImage(
                          item,
                        ),
                      ),
                    )
                    .toList(),
              )),
          const ListTile(
            leading: Icon(Icons.arrow_drop_down_circle),
            title: Text('Rex', style: TextStyle(color: Colors.blueGrey)),
          ),
          Card(
              clipBehavior: Clip.antiAlias,
              margin: const EdgeInsets.all(10.0),
              child: Column(
                children: const <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 2.0),
                      child: FittedBox(
                        child: Text('Ideal Weight:',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black87,
                                fontSize: 15)),
                      )),
                  Padding(
                      padding: EdgeInsets.only(top: 2.0),
                      child: ListTile(
                        leading: MyBullet(),
                        title: Text('27kg - 34kg',
                            textAlign: TextAlign.left,
                            style:
                                TextStyle(color: Colors.black87, fontSize: 12)),
                      ))
                ],
              )),
          Card(
              clipBehavior: Clip.antiAlias,
              margin: const EdgeInsets.all(10.0),
              child: Column(
                children: const <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 2.0),
                      child: FittedBox(
                        child: Text('Food:',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black87,
                                fontSize: 15)),
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 2.0),
                    child: ListTile(
                      leading: MyBullet(),
                      title: Text('Drink water every day',
                          textAlign: TextAlign.start,
                          style:
                              TextStyle(color: Colors.black87, fontSize: 12)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 2.0),
                    child: ListTile(
                      leading: MyBullet(),
                      title: Text('Eat 2 times a day',
                          textAlign: TextAlign.start,
                          style:
                              TextStyle(color: Colors.black87, fontSize: 12)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 2.0),
                    child: ListTile(
                      leading: MyBullet(),
                      title: Text('Eat between 420g and 525g of food per day',
                          textAlign: TextAlign.start,
                          style:
                              TextStyle(color: Colors.black87, fontSize: 12)),
                    ),
                  )
                ],
              )),
          Card(
              clipBehavior: Clip.antiAlias,
              margin: const EdgeInsets.all(10.0),
              child: Column(
                children: const <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 2.0),
                      child: FittedBox(
                        child: Text('Exercise:',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black87,
                                fontSize: 15)),
                      )),
                  Padding(
                      padding: EdgeInsets.only(top: 2.0),
                      child: ListTile(
                        leading: MyBullet(),
                        title: Text('Exercise regularly',
                            textAlign: TextAlign.start,
                            style:
                                TextStyle(color: Colors.black87, fontSize: 12)),
                      ))
                ],
              )),
        ]),
      ),
    );
  }
}

class MyBullet extends StatelessWidget {
  const MyBullet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 5.0,
      width: 5.0,
      decoration: const BoxDecoration(
        color: Colors.black,
        shape: BoxShape.circle,
      ),
    );
  }
}
