// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mypetcare/data/models/hourlyForecast.dart';
import 'package:mypetcare/logic/blocs/weather/weather_bloc.dart';
import 'package:mypetcare/presentation/screens/hamburgerMenu_view.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<WeatherBloc>(context).add(
      WeatherRequested(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                HamburgerMenu(),
              ],
            ),
          ),
        ),
      ),
      //Comentar Lienas

      appBar: AppBar(
        // The title text which will be shown on the action bar
        title: (Text('My Pet Care')),
        backgroundColor: Color.fromRGBO(12, 116, 137, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.account_circle),
            onPressed: () {
              Navigator.of(context).pushNamed('/userProfile');
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.only(top: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                //_textHeader(),
                _pets(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                  Text('Your Apointments',
                    style: TextStyle(
                      fontSize: 36,
                    )),
                    FloatingActionButton.small(  
                      child: Icon(Icons.add),  
                      backgroundColor: Color.fromRGBO(12, 116, 137, 1),  
                      foregroundColor: Colors.white,  
                      onPressed: (){
                        Navigator.of(context).pushNamed('/addEvent');},  
      ),  
                ],),
                
                //_addEvents(),
                //_commingEvents(),
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Suggestions',
                          style: TextStyle(
                            fontSize: 36,
                          )),
                      IconButton(
                        icon: Icon(Icons.refresh_rounded),
                        onPressed: () {
                          BlocProvider.of<WeatherBloc>(context)
                              .add(WeatherRequested());
                        },
                      ),
                    ]),

                Container(
                    margin: const EdgeInsets.all(10.0),
                    child: BlocBuilder<WeatherBloc, WeatherState>(
                        builder: (context, state) {
                    
                      if (state is WeatherLoading) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      } else if (state is WeatherObtained) {
                        return Column(
                          children: <Widget>[
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Current temperature: " +
                                      state.temperatureC.toString() +
                                      "°C  "),
                                ]),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(state.weatherRecommendation,
                                      style: TextStyle(
                                          color: Color.fromARGB(
                                              221, 113, 113, 113)))
                                ]),
                            Container(
                              padding: EdgeInsets.only(top: 2.0),
                              child: SizedBox(
                                height: 130,
                                child: Scrollbar(
                                  child: ListView(
                                      scrollDirection: Axis.horizontal,
                                      children:
                                          displaySuggestion(state.bestHours)),
                                ),
                              ),
                            ),
                            Text(
                                "Last updated: " +
                                    state.lastUpdate.substring(
                                        0, state.lastUpdate.indexOf('.')),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color.fromARGB(221, 113, 113, 113))),
                          ],
                        );
                      } else if (state is WeatherFailure) {
                        Text(
                            "Impossible to calculate suggestions while offline. Please verify your internet connection and try again",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color.fromARGB(221, 113, 113, 113)));
                      }
                      return Container();
                    })),
              ],
            )),
      ),
    );
  }


  List<Container> displaySuggestion(List<HourlyForecast> bestHours) {
    List<Container> recommendedHours = [];
    String iconPath = '';
    for (int i = 0; i < bestHours.length; i++) {
      iconPath = bestHours[i].conditionCode.toString();
      recommendedHours.add(Container(
        width: 150,
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Column(
              children: [
                Row(children: <Widget>[
                  Image(
                      image: AssetImage('assets/icons/weather/$iconPath.png')),
                  Expanded(
                    child: Text(
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        bestHours[i].conditionText),
                  )
                ]),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text(bestHours[i].temperature.toString() + "°C",
                      style: TextStyle(
                        fontSize: 30,
                      ))
                ]),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text(
                      DateTime.parse(bestHours[i].date).hour.toString() + ":00")
                ])
              ],
            )),
      ));
    }
    return recommendedHours;
  }

  //Display Pet image
    Widget _pets(){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 10.0),
      child: Container(
        decoration: _boxDecoration(),
        height: 250.0,
        width: 250,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(children: <Widget>[
              _petimage(),
            ]),
            Row(
              children: <Widget>[
                Icon(Icons.pets),
                Text(' Next Meal in 1 hour',
                    style: TextStyle(color: Colors.white)),
              ],
            ),
            Row(
              children: <Widget>[
                Icon(Icons.event_available),
                Text(' Next apointment in 3 days',
                    style: TextStyle(color: Colors.white)),
              ],
            ),
          ],
        ),
      ),
    );
  }

  BoxDecoration _boxDecoration() {
    return BoxDecoration(
      color: Color.fromRGBO(170, 62, 152, 1),
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          offset: Offset(2.0, 2.0),
          blurRadius: 8.0,
        ),
      ],
    );
  }

  Widget _petimage() {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 10.0),
        child: Container(
            decoration: _boxDecorationPet(),
            height: 150,
            width: 150,
            child: Column(children: <Widget>[
              Image(
                  image: NetworkImage(
                "https://cdn-icons-png.flaticon.com/512/194/194177.png",
              ))
            ])));
  }

  BoxDecoration _boxDecorationPet() {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(100.0),
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.black26,
          offset: Offset(2.0, 2.0),
          blurRadius: 8.0,
        ),
      ],
    );
  }
}
