// ignore_for_file: library_private_types_in_public_api
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:mypetcare/logic/blocs/authentication/authentication_bloc.dart';
import 'package:mypetcare/logic/cubits/connectivity/connectivity_cubit.dart';

class SignUpView extends StatefulWidget {
  const SignUpView({Key? key}) : super(key: key);

  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  String _email = '';
  String _password = '';
  String _phoneNumber = '';
  String _fullName = '';
  String _birthDate = '';
  bool connect = false;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(12, 116, 137, 1),
          title: const Text("MY PET CARE"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [MultiBlocListener(listeners:[ 
              BlocListener<AuthenticationBloc, AuthenticationState>(listener: (context, state) {
            if (state is Authenticated) {
              Navigator.of(context).pushNamed('/home');
            }
            if (state is AuthError) {
              // Showing the error message if the user has entered invalid credentials
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text(state.error)));
            }
          }), 
           BlocListener<ConnectivityCubit, ConnectivityState>(
                      listener: (context, state) {
                          if (state is InternetDisconnected) {
                            connect = false;
                          ScaffoldMessenger.of(context)
                                .showSnackBar(SnackBar(content: Text('No internet connection')));
                          }else{connect = true;}                      
                      }),
                       BlocListener<ConnectivityCubit, ConnectivityState>(
                        listenWhen: (previous,current){
                          return previous is InternetDisconnected && current is InternetConnected;
                        },
                        listener: (context, state) {
                          if (state is InternetDisconnected) {
                             connect = false;
                          ScaffoldMessenger.of(context)
                                .showSnackBar(SnackBar(content: Text('You are now connected to the internet')));
                          }else{connect = true;}
                      }),
          ], child:SizedBox.shrink()),
          BlocBuilder<AuthenticationBloc, AuthenticationState>(
               builder: (context, state) {
              if (state is AuthLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (state is UnAuthenticated) {
                return Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 60.0),
                          child: Center(
                            child: Container(
                                width: 250,
                                height: 150,
                                child: const Text('Welcome to My Pet Care!',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black87, fontSize: 40))),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextFormField(
                            maxLength: 35,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'E-mail',
                              floatingLabelBehavior: FloatingLabelBehavior.always,
                            ),
                            validator: (text) {
                              if (text == null || text.isEmpty) {
                                return 'Can\'t be empty';
                              }
                              if (text.length < 4) {
                                return 'Too short';
                              }
                              if (!EmailValidator.validate(text)) {
                                return 'Must be a valid email address';
                              }
                              return null;
                            },
                            onChanged: (text) => setState(() => _email = text),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 15, bottom: 0),
                          child: TextFormField(
                            maxLength: 20,
                            obscureText: true,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Password',
                              floatingLabelBehavior: FloatingLabelBehavior.always,
                            ),
                            validator: (text) {
                              if (text == null || text.isEmpty) {
                                return 'Can\'t be empty';
                              }
                              if (text.length < 6) {
                                return 'Too short!';
                              }
                              return null;
                            },
                            onChanged: (text) => setState(() => _password = text),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 15, bottom: 0),
                          //padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextFormField(
                            maxLength: 25,
                            obscureText: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Full Name',
                              floatingLabelBehavior: FloatingLabelBehavior.always,
                            ),
                            validator: (text) {
                              if (text == null || text.isEmpty) {
                                return 'Can\'t be empty';
                              }
                              if (text.length < 4) {
                                return 'Too short';
                              }
                              return null;
                            },
                            onChanged: (text) => setState(() => _fullName = text),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 15, bottom: 0),
                          //padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextFormField(
                            maxLength: 10,
                            keyboardType: TextInputType.number,
                            obscureText: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Phone Number',
                              floatingLabelBehavior: FloatingLabelBehavior.always,
                            ),
                            validator: (text) {
                              if (text == null || text.isEmpty) {
                                return 'Can\'t be empty';
                              }
                              if (text.length < 10) {
                                return 'Too short';
                              }
                              return null;
                            },
                            onChanged: (text) =>
                                setState(() => _phoneNumber = text),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 15, bottom: 15),
                          child: TextFormField(
                            controller: TextEditingController(text: _birthDate),
                            decoration: InputDecoration(
                              icon: Icon(Icons.calendar_today), //icon of text field
                              labelText: "Birth Date", //label text of field
                              floatingLabelBehavior: FloatingLabelBehavior.always,
                            ),
                            validator: (text) {
                              if (text == null || text.isEmpty) {
                                return 'Can\'t be empty';
                              }
                              return null;
                            },
                            readOnly: true,
                            //set it true, so that user will not able to edit text
                            onTap: () async {
                              DateTime? pickedDate = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(1900),
                                  //DateTime.now() - not to allow to choose before today.
                                  lastDate: DateTime.now());
          
                              if (pickedDate != null) {
                                print(pickedDate); //ej=> 2021-03-10 00:00:00.000
                                String formattedDate =
                                    DateFormat('dd/MM/yy').format(pickedDate);
                                print(formattedDate); //ej=>  03/10/21
                                setState(() {
                                  _birthDate =
                                      formattedDate; //set output date to TextField value.
                                });
                              }
                            },
                          ),
                        ),
                        Container(
                          height: 50,
                          width: 250,
                          child: ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Color.fromRGBO(12, 116, 137, 1),
                                ),
                                shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(100)))),
                            onPressed: () {
                              if(connect == true){
                              _authenticateWithEmailAndPassword(context);
                              }
                              else{
                                ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(content: Text('You don\'t have internet connection. \n Check your internet connection and try again')));
                              }   
                            },
                            child: const Text(
                              'SignUp',
                              style: TextStyle(color: Colors.white, fontSize: 25),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.of(context).pushNamed('/');
                          },
                          child: const Text(
                            "Already have an account?",
                            style: TextStyle(color: Colors.grey, fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  );
                
              }
              return Container();
            }),
          ]),
        ));
  }

  void _authenticateWithEmailAndPassword(context) {
    if (_formKey.currentState!.validate()) {
      // If email is valid adding new event [SignUpRequested].
      BlocProvider.of<AuthenticationBloc>(context).add(
        SignUpRequested(
          _email,
          _password,
          _birthDate,
          _phoneNumber,
          _fullName,
        ),
      );
    }
  }
}
